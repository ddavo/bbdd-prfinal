SET DEFINE OFF;

INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1001, 'Political');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1002, 'Card Game');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1009, 'Abstract Strategy');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1010, 'Fantasy');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1013, 'Farming');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1015, 'Civilization');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1016, 'Science Fiction');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1017, 'Dice');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1019, 'Wargame');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1020, 'Exploration');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1021, 'Economic');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1022, 'Adventure');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1023, 'Bluffing');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1024, 'Horror');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1025, 'Word Game');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1026, 'Negotiation');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1027, 'Trivia');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1029, 'City Building');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1030, 'Party Game');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1031, 'Racing');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1032, 'Action / Dexterity');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1033, 'Mafia');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1034, 'Trains');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1035, 'Medieval');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1036, 'Prehistoric');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1037, 'Real-time');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1038, 'Sports');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1039, 'Deduction');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1040, 'Murder/Mystery');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1041, 'Children''s Game');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1042, 'Expansion for Base-game');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1045, 'Memory');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1046, 'Fighting');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1047, 'Miniatures');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1049, 'World War II');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1050, 'Ancient');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1051, 'Napoleonic');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1052, 'Arabian');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1054, 'Music');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1055, 'American West');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1059, 'Maze');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1064, 'Movies / TV / Radio theme');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1065, 'World War I');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1069, 'Modern Warfare');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1070, 'Renaissance');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1072, 'Electronic');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1079, 'Humor');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1081, 'Spies/Secret Agents');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1084, 'Environmental');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1086, 'Territory Building');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1088, 'Industry / Manufacturing');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1089, 'Animals');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1090, 'Pirates');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1093, 'Novel-based');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1094, 'Educational');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1097, 'Travel');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1113, 'Space Exploration');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1116, 'Comic Book / Strip');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1118, 'Mature / Adult');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (1120, 'Print & Play');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (2145, 'Medical');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (2481, 'Zombies');
INSERT INTO ASCII_Categorias (CatId, Nombre)
    VALUES (2650, 'Aviation / Flight');
