ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601
ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS'; -- ISO 8601

-- 1. Obtener todas las versiones distintas del juego Trivial (Juegos que contengan la palabra Trivial)
SELECT codigo, nombre
FROM ASCII_Juegos
WHERE lower(nombre) LIKE '%trivial%';

-- 2. Devuelve el nombre (sin apellidos), la fecha de préstamo y devolución y el nombre del juego de los préstamos que no se
-- hayan entregado a tiempo. (Se han entregado después de las 8 de la tarde del día en el que se prestó).
SELECT usuarios.nombre, fprestamo, fdevolucion, juegos.nombre nombreJuego
FROM ASCII_Prestamos JOIN ASCII_Usuarios usuarios USING (Carnet) JOIN ASCII_Juegos juegos USING (Codigo)
WHERE fDevolucion > TO_TIMESTAMP(TO_CHAR(TO_DATE(fPrestamo), 'YYYY-MM-DD')||' 20:00:00');

-- 3. Devuelve el pseudónimo, nombre del juego y número de veces jugadasde todos los usuarios que hayan jugado más de 2 veces al
-- mismo juego en el mismo año. También puedes mostrar el año si quieres.
WITH res AS (
    SELECT carnet, codigo, count(*) cnt, extract(year from fprestamo) anio
    FROM ASCII_Prestamos
    GROUP BY carnet, codigo, extract(year from fprestamo)
    HAVING count(*) >= 2
)
SELECT apodo, juegos.nombre, cnt, anio
FROM res JOIN ASCII_Usuarios USING (carnet) JOIN ASCII_Juegos juegos USING (codigo);

-- wtf donde está datepart
-- vale, es extract()

-- 4. Obtener el nombre (con apellidos y pseudónimo) de todos los usuarios que lleven más de 5 años en la asociación
-- y el número de juegos distintos jugados.
SELECT nombre, apellidos, apodo, nJuegos, fAlta
FROM ASCII_Usuarios JOIN (
    SELECT carnet, count(DISTINCT codigo) nJuegos
    FROM ASCII_Prestamos
    GROUP BY carnet
) USING (carnet)
WHERE fAlta < CURRENT_DATE - 5*365;

-- 5. Obtener el nombre de los juegos que no ha jugado nunca nadie
SELECT nombre
FROM ASCII_Juegos
WHERE codigo NOT IN (SELECT codigo FROM ASCII_Prestamos);

-- 6. Obtener un top de los 20 juegos más jugados
SELECT nombre, count(carnet) cnt
FROM ASCII_Juegos LEFT JOIN (ASCII_Prestamos) USING (codigo)
GROUP BY codigo, nombre
ORDER BY cnt DESC
FETCH FIRST 20 ROWS ONLY;