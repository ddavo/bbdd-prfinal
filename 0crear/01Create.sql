CREATE TABLE ASCII_Materiales (
    Codigo      NUMBER(10) NOT NULL,
    Tipo        VARCHAR2(14) NOT NULL,
    Adquisicion TIMESTAMP NOT NULL,
    Estado      VARCHAR2(10) NOT NULL,
    Descripcion VARCHAR2(280),
    
    CONSTRAINT ascii_mat_PK PRIMARY KEY (Codigo),
    CONSTRAINT ascii_mat_check_Tipo CHECK (Tipo IN ('Cableado', 'Herramientas', 'Electrónica', 'Juegos')),
    CONSTRAINT ascii_mat_check_Estado CHECK (Estado IN ('Usable', 'Arreglable', 'Incunable', 'Lozano'))
);

CREATE TABLE ASCII_Juegos (
    Codigo          NUMBER(10) NOT NULL,
    Nombre          VARCHAR2(40) NOT NULL,
    MinJugadores    NUMBER(2),
    MaxJugadores    NUMBER(2),
    BGGId           NUMBER(8),
    
    CONSTRAINT ascii_juegos_PK PRIMARY KEY (Codigo),
    CONSTRAINT ascii_juegos_FK_Mat FOREIGN KEY (Codigo) REFERENCES ASCII_Materiales,
    CONSTRAINT ascii_juegos_check_jugadores CHECK (MinJugadores <> 0 AND MaxJugadores <> 0 AND MinJugadores <= MaxJugadores)
);

CREATE TABLE ASCII_Expansiones (
    Codigo          NUMBER(10) NOT NULL,
    Titulo          VARCHAR2(40) NOT NULL,
    
    CONSTRAINT ascii_exp_PK PRIMARY KEY(Codigo, Titulo),
    CONSTRAINT ascii_exp_FK FOREIGN KEY (Codigo) REFERENCES ASCII_Juegos
);

CREATE TABLE ASCII_Usuarios (
    Carnet          NUMBER (20) NOT NULL,
    FAlta           DATE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    Nombre          VARCHAR2(20),
    Apellidos       VARCHAR2(20),
    Apodo           VARCHAR2(20),
    
    CONSTRAINT ascii_user_PK PRIMARY KEY (Carnet)
);

CREATE TABLE ASCII_Prestamos (
    FPrestamo       TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    Carnet          NUMBER(20) NOT NULL,
    Codigo          NUMBER(10) NOT NULL,
    FDevolucion     TIMESTAMP DEFAULT NULL,
    
    CONSTRAINT ascii_pre_PK PRIMARY KEY (FPrestamo, Carnet),
    CONSTRAINT ascii_pre_FK_user FOREIGN KEY (Carnet) REFERENCES ASCII_Usuarios,
    CONSTRAINT ascii_pre_FK_mat FOREIGN KEY (Codigo) REFERENCES ASCII_Materiales,
    CONSTRAINT ascii_pre_check_dates CHECK (FDevolucion IS NULL OR FPrestamo < FDevolucion)
);

CREATE TABLE ASCII_Recomendaciones (
    Carnet          NUMBER(20) NOT NULL,
    Base            NUMBER(10) NOT NULL,
    Recomendacion   NUMBER(10) NOT NULL,
    
    CONSTRAINT ascii_rec_pk PRIMARY KEY (Carnet, Base, Recomendacion),
    CONSTRAINT ascii_rec_fk_user FOREIGN KEY (Carnet) REFERENCES ASCII_Usuarios,
    CONSTRAINT ascii_rec_fk_base FOREIGN KEY (Base) REFERENCES ASCII_Juegos,
    CONSTRAINT ascii_rec_fk_reco FOREIGN KEY (Recomendacion) REFERENCES ASCII_Juegos
);

CREATE TABLE ASCII_Categorias (
    CatID           NUMBER(5) NOT NULL,
    Nombre          VARCHAR2(30) NOT NULL,
    
    CONSTRAINT ascii_cat_pk PRIMARY KEY (CatID)
);

CREATE TABLE ASCII_CategoriasJuegos (
    Codigo          NUMBER(10) NOT NULL,
    CatID           NUMBER(5) NOT NULL,
    
    CONSTRAINT ascii_catjue_pk PRIMARY KEY (Codigo, CatID),
    CONSTRAINT ascii_catjue_fk_jue FOREIGN KEY (Codigo) REFERENCES ASCII_Juegos,
    CONSTRAINT ascii_catjue_fj_cat FOREIGN KEY (CatID) REFERENCES ASCII_Categorias
);
    